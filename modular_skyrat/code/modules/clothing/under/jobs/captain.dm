//captain pajamas (epic)
/obj/item/clothing/under/rank/captain
	icon = 'modular_skyrat/icons/obj/clothing/captain.dmi'
	mob_overlay_icon = 'modular_skyrat/icons/mob/clothing/captain.dmi'
	icon_state = "captain"
	desc = "Hail to the king, baby."
	mutantrace_variation = STYLE_NO_ANTHRO_ICON
