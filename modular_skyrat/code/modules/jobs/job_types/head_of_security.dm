/datum/job/hos
	title = "Chief Enforcer"

/datum/outfit/job/hos
	backpack_contents = list(/obj/item/pda/heads/hos=1,
							/obj/item/melee/mace=1,
							/obj/item/ammo_box/magazine/nangler=1)
	belt = /obj/item/storage/belt/sabre/hos
	suit_store = /obj/item/gun/ballistic/automatic/pistol/nangler
