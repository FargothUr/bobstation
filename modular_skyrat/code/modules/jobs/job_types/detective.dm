/datum/job/detective
	title = "Deputy"

/datum/outfit/job/detective
	name = "Deputy"
	backpack_contents = list(/obj/item/storage/box/evidence=1,\
		/obj/item/detective_scanner=1,\
		/obj/item/melee/classic_baton=1,\
		/obj/item/gun/ballistic/revolver/dual_ammo=1,\
		)
