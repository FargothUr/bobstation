// scream for more
/mob/living/carbon/human/agony_scream()
	return dna?.species?.agony_scream(src)

// gargle
/mob/living/carbon/human/agony_gargle()
	return dna?.species?.agony_gargle(src)

// death rattle
/mob/living/carbon/human/death_rattle()
	return dna?.species?.death_rattle(src)
