/obj/structure/door_assembly/door_assembly_public
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_glass.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_glass_overlays.dmi'
	icon_state = "construction"

/obj/structure/door_assembly/door_assembly_com
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_command.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'
	icon_state = "construction"

/obj/structure/door_assembly/door_assembly_sec
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_security.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'
	icon_state = "construction"

/obj/structure/door_assembly/door_assembly_eng
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_engineering.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'
	icon_state = "construction"

/obj/structure/door_assembly/door_assembly_min
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_mining.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_maint_overlays.dmi'
	icon_state = "construction"

/obj/structure/door_assembly/door_assembly_atmo
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_atmospherics.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'
	icon_state = "construction"

/obj/structure/door_assembly/door_assembly_research
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_research.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'
	icon_state = "construction"

/obj/structure/door_assembly/door_assembly_science
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_science.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'
	icon_state = "construction"

/obj/structure/door_assembly/door_assembly_med
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_medical.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'
	icon_state = "construction"

/obj/structure/door_assembly/door_assembly_mai
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_maint_extra.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_maint_overlays.dmi'
	icon_state = "construction"

/obj/structure/door_assembly/door_assembly_extmai
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_external.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_external_overlays.dmi'
	icon_state = "construction"

/obj/structure/door_assembly/door_assembly_ext
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_external.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_external_overlays.dmi'
	icon_state = "construction"

/obj/structure/door_assembly/door_assembly_fre
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_freezer.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'
	icon_state = "construction"

/obj/structure/door_assembly/door_assembly_hatch
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_cc_hatch.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_maint_overlays.dmi'
	icon_state = "construction"

/obj/structure/door_assembly/door_assembly_mhatch
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_maint.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_maint_overlays.dmi'
	icon_state = "construction"

/obj/structure/door_assembly/door_assembly_highsecurity
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_hightechsecurity.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_hightechsecurity_overlays.dmi'
	icon_state = "construction"

/obj/structure/door_assembly/door_assembly_vault
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_vault.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_vault_overlays.dmi'
	icon_state = "construction"

/obj/structure/door_assembly/door_assembly_shuttle
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_silver.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'
	icon_state = "construction"

/obj/structure/door_assembly/door_assembly_viro
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_medical_virology.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'
	icon_state = "construction"

/obj/structure/door_assembly/door_assembly_centcom
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_cc.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'
	icon_state = "construction"

/obj/structure/door_assembly/door_assembly_grunge
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_cc.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'
	icon_state = "construction"

/obj/structure/door_assembly/door_assembly_gold
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_material.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'
	color = "#ffff73"

/obj/structure/door_assembly/door_assembly_silver
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_material.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'
	color = "#a1c8db"

/obj/structure/door_assembly/door_assembly_diamond
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_material.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'
	color = "#3cf9ff"

/obj/structure/door_assembly/door_assembly_uranium
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_material.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'
	color = "#2fb314"

/obj/structure/door_assembly/door_assembly_plasma
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_material.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'
	color = "#d341ff"

/obj/structure/door_assembly/door_assembly_bananium
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_material.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'
	color = "#fffb00"

/obj/structure/door_assembly/door_assembly_sandstone
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_material.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'
	color = "#fff7b3"

/obj/structure/door_assembly/door_assembly_titanium
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_material.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'
	color = "#c7c7c7"

/obj/structure/door_assembly/door_assembly_wood
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_material.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'
	color = "#815400"
